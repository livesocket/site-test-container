FROM node:alpine as release
RUN apk update && apk upgrade \
	&& echo @edge http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories \
	&& echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories \
	&& apk add --no-cache --update \
	chromium@edge \
	nss@edge \
	freetype@edge \
	harfbuzz@edge \
	python \
	openssl \
	libgcc \
	make \
	libstdc++ \
	g++ \
	ca-certificates \
	wget \
	tar
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true
RUN npm install puppeteer@1.5.0 mocha
RUN addgroup -S chrome && adduser -S -g chrome chrome \
	&& mkdir -p /home/chrome/Downloads \
	&& chown -R chrome:chrome /home/chrome \
	&& chown -R chrome:chrome /node_modules
